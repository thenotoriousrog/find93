package find93.find93;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.github.florent37.bubbletab.BubbleTab;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  {

    private BubbleTab bubbleTab;
    private ViewPager viewPager;
    private BubbleFragmentStatePagerAdapter fragmentStatePagerAdapter;
    private ArrayList<Fragment> fragments; // the list of fragments that we are using

    private AddGasStationFragment addGasStationFragment;
    private CalculateOctaneMixtureFragment calculateOctaneMixtureFragment;
    private Find93OctaneMapFragment find93OctaneMapFragment;
    private ReportGasStationFragment reportGasStationFragment;
    private SettingsFragment settingsFragment;

    // sets up the view pager with the fragments needed for the application.
    private void setupViewPager() {

        fragments = new ArrayList<>();

        // initialize all fragments.
        find93OctaneMapFragment = new Find93OctaneMapFragment();
        calculateOctaneMixtureFragment = new CalculateOctaneMixtureFragment();
        addGasStationFragment = new AddGasStationFragment();
        reportGasStationFragment = new ReportGasStationFragment();
        settingsFragment = new SettingsFragment();

        // add fragments to list.
        fragments.add(find93OctaneMapFragment);
        fragments.add(calculateOctaneMixtureFragment);
        fragments.add(addGasStationFragment);
        fragments.add(reportGasStationFragment);
        fragments.add(settingsFragment);

        fragmentStatePagerAdapter = new BubbleFragmentStatePagerAdapter(getSupportFragmentManager(), fragments); // create the fragment state adapter needed for the view pager.

        // set the view pager properties and setup.
        viewPager.setAdapter(fragmentStatePagerAdapter);
        viewPager.setCurrentItem(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bubbleTab = findViewById(R.id.bubbleTab);
        viewPager = findViewById(R.id.viewPager);

        setupViewPager(); // setup the view pager and fragments that we are going to be using.

       bubbleTab.setupWithViewPager(viewPager); // complete the setup for the bubbleTab layout.
    }

    /*

     */
    @Override
    public void onBackPressed() {

    }
}
