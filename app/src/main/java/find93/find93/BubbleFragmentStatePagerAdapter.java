package find93.find93;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/*
    * The custom fragment state pager adapter for our main layout.
 */
public class BubbleFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> fragments; // list of all of the fragments that we are working with.
    public BubbleFragmentStatePagerAdapter(FragmentManager manager, ArrayList<Fragment> fragments) {
        super(manager);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return fragments.get(0); // returns Find93OctaneMapFragment

            case 1:
                return fragments.get(1); // returns CalculateOctaneMixtureFragment.

            case 2:
                return fragments.get(2); // returns AddGasStationFragment.

            case 3:
                return fragments.get(3); // returns ReportGasStationFragment.

            case 4:
                return fragments.get(4); // returns SettingsFragment.

            default:
                return fragments.get(0); // default to Find93OctaneMapFragment.
        }
    }

    // returns the size of all of our fragments.
    @Override
    public int getCount() {
        return fragments.size();
    }

}
