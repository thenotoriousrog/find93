package find93.find93;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/*
    * This fragment handles the logic associated with the calculated octane mixture fragment.
 */
public class CalculateOctaneMixtureFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.calculate_octance_fragment, container, false);

        // todo: implement me

        return mainView;
    }

}
